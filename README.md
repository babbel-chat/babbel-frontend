# Babbel-Frontend

## Requiements

You need [Node](https://nodejs.org/en/) on you local machine, we recommend latest LTS version 14.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build for production

Run `npm run build --prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Build with Docker

Run `docker build -t babbel-frontend:latest .` to build the project using docker. The image can then be run with `docker run -p 8081:80 babbel-frontend:latest`

The application will run on a [nginx web server](https://www.nginx.com/) which can be configured with the `nginx.conf` file in the project.

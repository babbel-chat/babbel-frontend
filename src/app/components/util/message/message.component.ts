import { Component, Input } from '@angular/core';
import { Message } from 'src/app/types';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent {

  constructor() { }

  @Input() message: Message | null = null;

  get isSystemMessage() : boolean {
    return this.message?.messageType == "SYSTEM"
  }

  get timestamp(): string {
    let timestamp = this.message?.timestamp
    if(timestamp && timestamp.length == 7){
      return `${timestamp[1]}/${timestamp[2]}/${timestamp[0]} ${timestamp[3]}:${timestamp[4]}`
    }
    return "n/a"
  }

}

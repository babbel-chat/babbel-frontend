import { Component, Input } from '@angular/core';
import { Message } from 'src/app/types';

@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.scss']
})
export class MessageBoardComponent {

  constructor() { }

  @Input() messages: Message[] | null = null;

}

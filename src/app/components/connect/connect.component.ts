import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../../services/chat.service';
import { UserService } from '../../services/user.service';
import { User } from '../../types';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.scss']
})
export class ConnectComponent {

  user: User = null

  constructor(private router: Router,
    private chatService: ChatService,
    private userService: UserService) { }

  connect(): void {
    this.userService.currentUser = this.user;
    this.router.navigate(['']);
  }
}

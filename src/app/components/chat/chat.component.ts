import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ChatService } from '../../services/chat.service';
import { Message, MessageText } from '../../types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  @ViewChild('messageBoard') private chatBoard: ElementRef;

  messages: Message[] = []
  message: MessageText = ""
  messageMaxLength: number = 300

  constructor(
    private chatService: ChatService, 
    private userService: UserService,
    private router: Router
    ) { }

  get charatersLeft() : number {
    return this.messageMaxLength - this.message.length
  }

  ngOnInit(): void {
    this.connect()
  }

  connect(): void {
    this.chatService.connect()
      .subscribe(message => {        
        this.messages.push(message);
        this.scrollToBottom();
      });
  }

  sendMessage(): void {
    this.chatService.sendMessage(this.message)
    this.message = ""
  }

  leave(): void {
    this.userService.clearUser();
    this.router.navigate(['connect']);
  }

  scrollToBottom(): void {
    try {
      if (this.chatBoard) {
        this.chatBoard.nativeElement.scrollTop = this.chatBoard.nativeElement.scrollHeight;
      }
    } catch (err) {
      console.log(err)
    }
  }

  onKeydown(event: any): void {
    if (event.keyCode === 13 && !event.shiftKey && this.message) {
      this.sendMessage()
      this.message = ""
    }
  }

  ngOnDestroy(): void {
    this.chatService.closeConnection();
  }
}

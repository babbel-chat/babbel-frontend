import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  localStorage: Storage;

  constructor() {
    this.localStorage = window.localStorage;
  }

  get(key: string): string | null {
    if (this.isLocalStorageSupported) {
      return this.localStorage.getItem(key);
    }
    return null;
  }

  set(key: string, value: string): boolean {
    if (this.isLocalStorageSupported) {
      this.localStorage.setItem(key, value);
      return true;
    }
    return false;
  }

  remove(key: string): any {
    if (this.isLocalStorageSupported) {
      this.localStorage.removeItem(key);
      return true;
    }
    return false;
  }

  exists(key: string): boolean {
    if (this.isLocalStorageSupported) {
      return !!this.get(key)
    }
    return false
  }

  private get isLocalStorageSupported(): boolean {
    return !!this.localStorage
  }
}

import { Injectable } from '@angular/core';
import { Message, MessageText } from '../types';
import { Observable } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { UserService } from './user.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  connection$: WebSocketSubject<any> | null = null;
  messageMaxLength: number = 300

  constructor(private userService: UserService) { }

  connect(): Observable<any> {
    this.connection$ = this.createConnection()        
    return this.connection$
  }

  private createConnection(): WebSocketSubject<any> {
    return webSocket({
      url: `${environment.protocol}://${environment.host}/chat/${this.userService.currentUser}`,
      deserializer: ( data ) => {
        let message: Message = <Message>JSON.parse(data.data)
        console.log(message)
        return message
      },
      serializer: ( data ) => {
        return data
      },
    })
  }

  sendMessage(message: MessageText): void {
    if (this.connection$) {
      this.connection$.next(message)
    }
    else {
      console.log('Did not send data, unable to open connection')
    }
  }

  closeConnection(): void {
      this.connection$?.complete();
  }
}

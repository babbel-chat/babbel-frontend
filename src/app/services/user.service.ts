import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { User } from '../types';

const connectedUserKey = "connectedUser"

@Injectable({
  providedIn: 'root'
})
export class UserService  {

  connectedUser: User = null

  constructor(private localStorageService: LocalStorageService) {
  }

  get currentUser(): User {
    if (this.connectedUser) {
      return this.connectedUser;
    }
    let cachedUser = this.localStorageService.get(connectedUserKey)
    if (cachedUser) {
      return atob(cachedUser)
    }
    return null
  }

  set currentUser(user: User) {
    this.connectedUser = user;
    if (user) {
      this.localStorageService.set(connectedUserKey, btoa(user));
    }
  }

  get isUserConnected(): boolean {
    return !!this.currentUser;
  }

  clearUser(): void {
    this.connectedUser = null
    this.localStorageService.remove(connectedUserKey)
  }
}

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class UserGuard implements CanActivate {

    constructor(public userService: UserService, public router: Router) { }

    canActivate(): boolean {
        if (this.userService.isUserConnected) {
            this.router.navigate(['chat']);
            return false;
        }
        return true;
    }
}
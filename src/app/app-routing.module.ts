import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChatComponent } from './components/chat/chat.component';
import { NoUserGuard } from './guards/no-user.guard';
import { ConnectComponent } from './components/connect/connect.component';
import { UserGuard } from './guards/user.guard';

const routes: Routes = [
  {
    path: 'chat',
    component: ChatComponent,
    canActivate: [NoUserGuard]
  },
  {
    path: 'connect',
    component: ConnectComponent,
    canActivate: [UserGuard]
  },
  {
    path: '**',
    redirectTo: 'chat'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatComponent } from './components/chat/chat.component';
import { ConnectComponent } from './components/connect/connect.component';
import { NoUserGuard } from './guards/no-user.guard';
import { UserGuard } from './guards/user.guard';
import { MessageComponent } from './components/util/message/message.component';
import { MessageBoardComponent } from './components/util/message-board/message-board.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    ConnectComponent,
    MessageComponent,
    MessageBoardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [NoUserGuard, UserGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

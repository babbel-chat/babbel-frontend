export type MessageText = string

export type Timestamp = string[];

export type Message = {
    username: User,
    timestamp: Timestamp,
    message: MessageText,
    messageType: string
}

export type User = string | null
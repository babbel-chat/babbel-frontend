# Stage I: compile and Build angular source code

FROM node:lts as build

WORKDIR /usr/local/app

COPY ./package.json ./package-lock.json ./

RUN npm install

COPY ./ .

RUN npm run build --prod

# Stage II: serve build angular app on nginx

FROM nginx:alpine

COPY --from=build /usr/local/app/dist/frontend /usr/share/nginx/html
#COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80